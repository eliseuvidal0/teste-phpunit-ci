<?php

declare(strict_types=1);

namespace Teste\src;

use Teste\src\Model\ModelWs;

class Treino
{
    public function primeiro(): string
    {
        return 'oi jovem!';
    }

    /**
     * @param array<mixed> $dados
     * @return array<mixed>
     */
    public static function chamarWs(array $dados): array
    {
        return ModelWs::calculoWsCorreios($dados);
    }
}

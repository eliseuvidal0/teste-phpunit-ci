<?php

declare(strict_types=1);

namespace Teste\src\Model;

class ModelWs
{
    /**
     * @param  array<mixed> $dados
     * @return array<mixed>
     */
    public static function calculoWsCorreios(array $dados): array
    {
        $solicitacao = http_build_query(
            [
                'nCdServico' => $dados['cod'],
                'sCepOrigem' => $dados['cepOrigem'],
                'sCepDestino' => $dados['cepDestino']
            ]
        );

        $url = "http://ws.correios.com.br/calculador/calcprecoprazo.asmx/CalcPrazo?";
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $solicitacao);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $resultado = curl_exec($curl);

        curl_close($curl);

        $xml = simplexml_load_string($resultado);
        $json = json_encode($xml);
        $final = json_decode($json,true);

        return $final;
    }
}

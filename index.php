<?php

declare(strict_types=1);

require_once 'vendor/autoload.php';
use Teste\src\Treino;

$dados = [
    'cod'        => '40010',
    'cepOrigem'  => '01250000',
    'cepDestino' => '81930310',
];

$retorno = Treino::chamarWs($dados);

echo '<pre>';
print_r($retorno);
echo '<hr>';
exit();
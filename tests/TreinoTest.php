<?php

declare(strict_types=1);

namespace Teste\tests;

use PHPUnit\Framework\TestCase;
use Teste\src\Treino;

class TreinoTest extends TestCase
{
    public function testPrimeiro(): void
    {
        $t = new Treino();
        $this->assertEquals('oi jovem!', $t->primeiro());
        self::assertFalse(false);
        self::assertTrue(true);
    }

    public function testChamarWs(): void
    {
        $dados = [
            'cod'        => '40010',
            'cepOrigem'  => '01250000',
            'cepDestino' => '81930310',
        ];
        $retorno = Treino::chamarWs($dados);
        self::assertIsArray($retorno);
        self::assertEmpty($retorno['Servicos']['cServico']['Erro']);
        self::assertArrayHasKey('Codigo', $retorno['Servicos']['cServico']);
        $dados = [
            'cod'        => '40010',
            'cepOrigem'  => '0125000',
            'cepDestino' => '8193031',
        ];
        $retorno = Treino::chamarWs($dados);
        self::assertNotEmpty(
            $retorno['Servicos']['cServico']['Erro'],
            ' => ERRO ao testar servico - calcprecoprazo - codErro - ' .
                $retorno['Servicos']['cServico']['Erro'] . ' - msg - ' . $retorno['Servicos']['cServico']['MsgErro']
            );
    }
}